package ex02;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("ex02")
@Configuration
public class AppConfig {

    @Bean
    TimeManager getTimeManager() {
        return new TimeManager();
    }

}
