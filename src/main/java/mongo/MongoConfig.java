package mongo;

import org.springframework.context.annotation.Bean;

public class MongoConfig {

    @Bean
    public MessageRepository getMongoRepository(){
        return new MessageRepository();
    }
}
