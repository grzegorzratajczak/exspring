package ex02;

import org.springframework.stereotype.Component;

@Component("randomNumber")
public class RandomNumberGenerator {

    public int generateNumber(int min, int max){
        int number = min + (int)(Math.random() * ((max - min) + 1));
        return number;
    }
}
