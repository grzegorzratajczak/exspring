package ex00;

import mongo.MongoConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("ex00")
@Import(MongoConfig.class)
public class AppConfig {

    @Bean
    MessageSender getMessageSender() {
        return new MessageSender();
    }
}
