package ex00;

import mongo.MessageRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext appCntx = new AnnotationConfigApplicationContext(AppConfig.class);
        MessageSender msgSender = appCntx.getBean(MessageSender.class);
        msgSender.sendMessage();

        UdpSender udpSender = appCntx.getBean(UdpSender.class);
        udpSender.send();

        MessageRepository repository = appCntx.getBean(MessageRepository.class);
        System.out.printf("Message repository is %snull",
                repository == null ? "" : "not ");
    }
}
