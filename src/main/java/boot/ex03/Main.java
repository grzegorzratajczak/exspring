package boot.ex03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    ApplicationContext appCntx = new AnnotationConfigApplicationContext(AppConfig.class);
    TimeManager timeManager = appCntx.getBean(TimeManager.class);
    RandomNumberGenerator randomNumber = appCntx.getBean(RandomNumberGenerator.class);

    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }

    @RequestMapping("/currentTime")
    String time() {
        return timeManager.giveHour();
    }

    @RequestMapping("/currentDay")
    String day() {
        return timeManager.giveDate();
    }

    @RequestMapping("/sum")
    String sum(@RequestParam String a, @RequestParam String b) throws InvalidDataException {
        if(a==null || b==null) {
            throw new InvalidDataException("Podałeś mniej niż 2 zmienne");
        }else {
            try{
                Integer a2 = Integer.parseInt(a);
                Integer b2 = Integer.parseInt(b);
                return String.valueOf(a2+b2);
            }catch (Exception e){
                return String.valueOf(new InvalidDataException("Złe zmienne"));
            }
        }
    }

    @RequestMapping("/randomNumber")
    int random(@RequestParam int a, @RequestParam int b) {
        return randomNumber.generateNumber(a, b);
    }
}