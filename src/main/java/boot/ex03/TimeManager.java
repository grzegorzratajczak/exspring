package boot.ex03;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class TimeManager {

    public String giveHour(){
        String hour = ZonedDateTime.now().toLocalTime().truncatedTo(ChronoUnit.SECONDS).toString();
        return hour;
    }

    public String giveDate(){
        String date = LocalDate.now().toString();
        return date;
    }

    public String giveLocalizedDate(ZoneId zone){
        String localizedDate = ZonedDateTime.now(zone).truncatedTo(ChronoUnit.SECONDS).toString();
        return localizedDate;
    }
}
