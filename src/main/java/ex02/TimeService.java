package ex02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneId;

@Service
public class TimeService {

    private TimeManager timeManager;

    @Autowired
    public TimeService(TimeManager timeManager) {
        this.timeManager = timeManager;
    }

    public String giveHour() {
        return timeManager.giveHour();
    }

    public String giveDate() {
        return timeManager.giveDate();
    }

    public String giveLocalizedDate(ZoneId zone) {
        return timeManager.giveLocalizedDate(zone);
    }
}
