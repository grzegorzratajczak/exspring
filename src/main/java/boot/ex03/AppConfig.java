package boot.ex03;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    TimeManager getTimeManager() {
        return new TimeManager();
    }

    @Bean
    RandomNumberGenerator getRandomNumberGenerator(){
        return new RandomNumberGenerator();
    }

}
