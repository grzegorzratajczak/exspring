package ex00;

import org.springframework.stereotype.Service;

@Service
public class UdpSender {

    MessageSender messageSender;

    public void send(){
        System.out.println("Message send from UdpSender");
    }
}
