package ex02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.ZoneId;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ApplicationContext appCntx = new AnnotationConfigApplicationContext(AppConfig.class);
        TimeManager timeManager = appCntx.getBean(TimeManager.class);
        TimeService timeService = appCntx.getBean(TimeService.class);

        System.out.println(timeManager.giveHour());
        System.out.println(timeManager.giveDate());
        System.out.println(timeManager.giveLocalizedDate(ZoneId.of("Asia/Tokyo")));

        System.out.println(timeService.giveHour());
        System.out.println(timeService.giveDate());
        System.out.println(timeService.giveLocalizedDate(ZoneId.of("Asia/Tokyo")));

        RandomNumberGenerator randomNumber = (RandomNumberGenerator)appCntx.getBean("randomNumber");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj zakres losowej liczby, min: ");
        int min = scanner.nextInt();
        System.out.println("Podaj max: ");
        int max = scanner.nextInt();
        System.out.println(randomNumber.generateNumber(min, max));
    }
}
