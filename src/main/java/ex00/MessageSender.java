package ex00;

import org.springframework.beans.factory.annotation.Autowired;

public class MessageSender {

    private UdpSender sender;

    @Autowired
    public void setSender(UdpSender sender) {
        this.sender = sender;
    }

    public void sendMessage(){
        System.out.println("Message send");
        System.out.println("Sender is null: " + (sender==null));
    }

}
